/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2017 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"
#include <gtk/gtk.h>
#include "helloworld-newport.h"
#include "helloworld-newport-window.h"

struct _HlwNewport
{
  /*< private >*/
  GtkApplication parent;
};

G_DEFINE_TYPE (HlwNewport, hlw_newport, GTK_TYPE_APPLICATION);

static void
hlw_newport_activate (GApplication *app)
{
  HlwNewportWindow *win = NULL;
  win = hlw_newport_window_new (HLW_NEWPORT (app));
  gtk_window_present (GTK_WINDOW (win));
}

static void
hlw_newport_class_init (HlwNewportClass *klass)
{
  G_APPLICATION_CLASS (klass)->activate = hlw_newport_activate;
}

static void
hlw_newport_init (HlwNewport *self)
{
}

HlwNewport *
hlw_newport_new (void)
{
  return g_object_new (HLW_TYPE_NEWPORT,
                       "application-id", "org.apertis.HelloWorld.Newport",
                       NULL);
}
