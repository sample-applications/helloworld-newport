/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2017 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"
#include <gtk/gtk.h>
#include <newport/newport.h>
#include "helloworld-newport.h"
#include "helloworld-newport-window.h"

#define WIN_WIDTH  480
#define WIN_HEIGHT 800

/* Push to a personal repo/branch and customize the values below to test changes to the images */
#define GITLAB_REPO "https://gitlab.apertis.org/sample-applications/helloworld-newport/-/raw/master"

static const gchar *image_files[] = {
  GITLAB_REPO "/helloworld-newport/resources/images/A-500.png",
  GITLAB_REPO "/helloworld-newport/resources/images/Apertis-500.png"
};

struct _HlwNewportWindow
{
  /*< private >*/
  GtkApplicationWindow parent;
  NewportService *newport;           /* owned */
  NewportDownload *newport_download;
  gchar *download_path;
  GtkWidget *image; /* unowned */
  guint cnt;
};

G_DEFINE_TYPE (HlwNewportWindow, hlw_newport_window, GTK_TYPE_APPLICATION_WINDOW);

static void newport_start_download_cb (GObject *source_object,
                                       GAsyncResult *res,
                                       gpointer user_data);

static gboolean timeout_cb (gpointer user_data)
{
  HlwNewportWindow *self = user_data;

  newport_service_call_start_download (self->newport,
                                       image_files[self->cnt++ % G_N_ELEMENTS (image_files)],
                                       self->download_path,
                                       NULL,
                                       newport_start_download_cb,
                                       self);

  return FALSE;
}

static void
newport_download_progress_cb (NewportDownload *object,
                              guint64 cur_size,
                              guint64 total_size,
                              guint64 download_speed,
                              guint64 elapsed_time,
                              guint64 remaining_time,
                              gpointer user_data)
{
  g_debug ("Downloading %" G_GINT64_FORMAT " / %" G_GINT64_FORMAT,
           cur_size,
           total_size);
}

static void
newport_download_state_cb (NewportDownload *newport_download,
                           GParamSpec *pspec,
                           gpointer user_data)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (GFile) file = NULL;
  HlwNewportWindow *self = user_data;
  guint state = newport_download_get_state (newport_download);
  const gchar *downloaded_path;

  if (state != NEWPORT_DOWNLOAD_STATE_SUCCESS)
    return;

  downloaded_path = newport_download_get_path (self->newport_download);

  g_debug ("Download completed (path: %s)", downloaded_path);

  gtk_image_set_from_file(GTK_IMAGE(self->image), downloaded_path);

  file = g_file_new_for_path (downloaded_path);
  g_file_delete (file, NULL, &error);

  g_clear_object (&self->newport_download);
  g_timeout_add (500, timeout_cb, self);
}

static void
newport_download_ready_cb (GObject *source_object,
                           GAsyncResult *res,
                           gpointer user_data)
{
  g_autoptr (GError) error = NULL;
  HlwNewportWindow *self = user_data;

  g_clear_object (&self->newport_download);
  self->newport_download =
      newport_download_proxy_new_for_bus_finish (res,
                                                 &error);
  if (error)
    {
      g_warning ("Failed to get Newport Download proxy (reason: %s", error->message);
      return;
    }

  g_signal_connect (self->newport_download,
                    "download-progress",
                    G_CALLBACK (newport_download_progress_cb),
                    self);

  g_signal_connect (self->newport_download,
                    "notify::state",
                    G_CALLBACK (newport_download_state_cb),
                    self);
}

static void
newport_start_download_cb (GObject *source_object,
                           GAsyncResult *res,
                           gpointer user_data)
{
  g_autoptr (GError) error = NULL;
  g_autofree gchar *download_obj_path = NULL;

  HlwNewportWindow *self = user_data;

  newport_service_call_start_download_finish (self->newport,
                                              &download_obj_path,
                                              res,
                                              &error);
  if (error)
    {
      g_warning ("Failed to start downloading (reason: %s)", error->message);
      return;
    }

  newport_download_proxy_new_for_bus (
      G_BUS_TYPE_SESSION,
      G_DBUS_PROXY_FLAGS_NONE,
      "org.apertis.HelloWorld.Newport.Newport",
      download_obj_path,
      NULL,
      newport_download_ready_cb,
      self);
}

static void
newport_service_ready_cb (GObject *source_object,
                          GAsyncResult *res,
                          gpointer user_data)
{
  g_autoptr (GError) error = NULL;
  HlwNewportWindow *self = user_data;

  self->newport = newport_service_proxy_new_for_bus_finish (
      res,
      &error);

  if (error)
    {
      g_warning ("Failed to get Newport service proxy. (reason: %s)", error->message);
      return;
    }

  newport_service_call_start_download (self->newport,
                                       image_files[self->cnt++ % 2],
                                       self->download_path,
                                       NULL,
                                       newport_start_download_cb,
                                       self);
}

static void
hlw_newport_window_init (HlwNewportWindow *self)
{
  HlwNewportWindow *win = HLW_NEWPORT_WINDOW(self); 
  gtk_window_set_default_size(GTK_WINDOW(win),WIN_WIDTH,WIN_HEIGHT);

  newport_service_proxy_new_for_bus (
      G_BUS_TYPE_SESSION,
      G_DBUS_PROXY_FLAGS_NONE,
      "org.apertis.HelloWorld.Newport.Newport",
      "/org/apertis/Newport/Service",
      NULL,
      newport_service_ready_cb,
      win);

  win->image = gtk_image_new();

  win->download_path = g_build_filename (
      g_get_user_special_dir (G_USER_DIRECTORY_DOWNLOAD),
      "newport-download",
      NULL);

  gtk_container_add (GTK_CONTAINER(win), win->image);
  gtk_widget_show_all (GTK_WIDGET(win));
}

static void
hlw_newport_window_class_init (HlwNewportWindowClass *klass)
{
}

HlwNewportWindow *
hlw_newport_window_new (HlwNewport *app)
{
  return g_object_new (HLW_NEWPORT_WINDOW_TYPE, "application", app, NULL);
}
